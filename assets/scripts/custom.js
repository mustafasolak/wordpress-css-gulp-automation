jQuery('.read-more').readmore({
    collapsedHeight: 300,
    speed: 500,
    embedCSS: false,
    moreLink: '<b><a href="#" class="text-dark text-uppercase read-more-text">Read More <i class="fas fa-chevron-down" aria-hidden="true"></i></a></b>',
    lessLink: '<b><a href="#" class="text-dark text-uppercase read-more-text">Read Less <i class="fas fa-chevron-up	" aria-hidden="true"></i></a></b>',
    beforeToggle: function(trigger, element, expanded) {
      if (expanded === false) {
        element.addClass('remove-after');
      } else {
        element.removeClass('remove-after');
      }
    }
  });