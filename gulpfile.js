const gulp = require('gulp');
const minifyCSS = require('gulp-csso');
const minifyImg = require('gulp-imagemin');
const minifyJS = require('gulp-uglify');
const concat = require('gulp-concat');
const del = require('del');
const runSequence = require('run-sequence');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var csslint = require('gulp-csslint');
var autoPrefixer = require('gulp-autoprefixer');
var cssComb = require('gulp-csscomb');
var cmq = require('gulp-merge-media-queries');
var cleanCss = require('gulp-clean-css');


//CSS

gulp.task('css',function(){
    gulp.src(['assets/styles/**/*.css'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(autoPrefixer())
        .pipe(cssComb())
        .pipe(cmq({log:true}))
        .pipe(csslint())
        .pipe(csslint.formatter())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('dist/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cleanCss({ compatibility: '*', format: 'keep-breaks' }))
        .pipe(gulp.dest('dist/'))
});
gulp.task('js',function(){
    gulp.src(['{jsSrc}/**/*.js'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(gulp.dest('{jsDest}'))
});




//JS
gulp.task('js', () => {
    return gulp.src('assets/scripts/*.js')
        .pipe(concat('main.min.js'))
        .pipe(minifyJS())
        .pipe(gulp.dest('dist/'))
});

//Images
gulp.task('img', () => {
    gulp.src('src/img/**/*')
        .pipe(minifyImg())
        .pipe(gulp.dest('dist/img'));
});

gulp.task('delete', () => del(['dist/*.css', 'dist/*.js', 'dist/img']));

gulp.task('watch', () => {
    gulp.watch("assets/styles/*.css", ['css']);
    gulp.watch("assets/scripts/*.js", ['js']);
    gulp.watch("assets/images/**/*", ['img']);
});

gulp.task('default', () => {
    runSequence(
        'delete',
        'css',
        'js',
        'img',
        'watch'
    );
});